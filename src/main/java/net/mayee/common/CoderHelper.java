package net.mayee.common;

import jodd.log.Logger;
import jodd.log.LoggerFactory;
import net.mayee.alice.common.Definiens;
import net.mayee.alice.entity.coder.SystemCodeItem;
import net.mayee.alice.entity.coder.UploadCodeItem;
import net.mayee.alice.entity.coder.UserStatusItem;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.util.*;

public class CoderHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(CoderHelper.class);
    private Map<String, SystemCodeItem> systemCodeItemMap;
    private Map<String, UserStatusItem> userStatusItemMap;
    private Map<String, UploadCodeItem> uploadCodeItemMap;

    private CoderHelper() {
        initCoderHelper();
    }

    private static final CoderHelper coderHelper = new CoderHelper();

    public static CoderHelper getInstance() {
        return coderHelper;
    }

    private void initCoderHelper() {
        loadCoderXml();
    }

    public Map<String, UploadCodeItem> getUploadCodeItemMap() {
        return uploadCodeItemMap;
    }

    public List<UserStatusItem> getUserStatusItemList() {
        return new ArrayList<UserStatusItem>(userStatusItemMap.values());
    }

    public String getTextBySystemCode(String code) {
        return systemCodeItemMap.get(code) == null ? "" : systemCodeItemMap.get(code).getText();
    }

    public String getTextByUserStatusCode(String code) {
        return userStatusItemMap.get(code) == null ? "" : userStatusItemMap.get(code).getText();
    }

    public UserStatusItem getObjectByUserStatusCode(String code) {
        return userStatusItemMap.get(code);
    }

    public SystemCodeItem getObjectBySystemCode(String code) {
        return systemCodeItemMap.get(code);
    }

    public Map<String, Object> buildCodeMap(String code) {
        Map<String, Object> reMap = new HashMap<String, Object>();
        reMap.put(Definiens.MSG_ID, code);
        reMap.put(Definiens.MSG_DESC, getTextBySystemCode(code));
        return reMap;
    }

    public Map<String, Object> buildDefSuccessMap() {
        return buildCodeMap(Definiens.SUCCESS_DEFAULT);
    }

    public Map<String, Object> buildDefFailMap() {
        return buildCodeMap(Definiens.ERR_SYS_01);
    }

    private void loadCoderXml() {
        String xmlPath = AliceHelper.getInstance().getJoddProps().getValue("system.coder.xml.path");
        SAXReader reader = new SAXReader();
        Document document;
        try {
            InputStream in = this.getClass().getResourceAsStream(xmlPath);
            document = reader.read(in);
        } catch (Exception e) {
            LOGGER.error("**** CoderHelper -> loadCoderXml fail! ****", e);
            return;
        }

        /* new or reset */
        systemCodeItemMap = new HashMap<String, SystemCodeItem>();
        userStatusItemMap = new HashMap<String, UserStatusItem>();
        uploadCodeItemMap = new HashMap<String, UploadCodeItem>();

        Element root = document.getRootElement();
        for (Iterator iterA = root.elementIterator(); iterA.hasNext(); ) {
            // 获取节点 systemCodeItems
            Element elementA = (Element) iterA.next();
            if (elementA != null && elementA.hasContent()) {
                if ("systemCodeItems".equals(elementA.getName())) {
                    // 获取节点 systemCodeItem
                    for (Iterator iterB = elementA.elementIterator(); iterB
                            .hasNext(); ) {
                        Element elementB = (Element) iterB.next();
                        if (elementB != null && "systemCodeItem".equals(elementB.getName())) {
                            Attribute attrB_code = elementB.attribute("code");
                            Attribute attrB_text = elementB.attribute("text");

                            // create systemCodeItem
                            SystemCodeItem systemCodeItem = new SystemCodeItem(
                                    attrB_code.getText(), attrB_text.getText());

                            Attribute attrB_defText = elementB.attribute("defText");
                            if (attrB_defText != null) {
                                systemCodeItem.setDefText(attrB_defText.getText());
                            } else {
                                systemCodeItem.setDefText("");
                            }
                            Attribute attrB_note = elementB.attribute("note");
                            if (attrB_note != null) {
                                systemCodeItem.setNote(attrB_note.getText());
                            } else {
                                systemCodeItem.setNote("");
                            }

                            systemCodeItemMap.put(attrB_code.getText(), systemCodeItem);
                        }
                    }// systemCodeItem end
                }// systemCodeItems end
                else if ("userStatusItems".equals(elementA.getName())) {
                    // 获取节点 userStatusItem
                    for (Iterator iterB = elementA.elementIterator(); iterB
                            .hasNext(); ) {
                        Element elementB = (Element) iterB.next();
                        if (elementB != null && "userStatusItem".equals(elementB.getName())) {
                            Attribute attrB_code = elementB.attribute("code");
                            Attribute attrB_text = elementB.attribute("text");
                            Attribute attrB_class = elementB.attribute("labelClass");
                            Attribute attrB_view = elementB.attribute("view");
                            if (attrB_view != null && "true".equals(attrB_view.getText())) {
                                continue;
                            }

                            // create userStatusItem
                            UserStatusItem userStatusItem = new UserStatusItem(
                                    attrB_code.getText(), attrB_text.getText(), attrB_class.getText());

                            Attribute attrB_defText = elementB.attribute("defText");
                            if (attrB_defText != null) {
                                userStatusItem.setDefText(attrB_defText.getText());
                            } else {
                                userStatusItem.setDefText("");
                            }
                            Attribute attrB_note = elementB.attribute("note");
                            if (attrB_note != null) {
                                userStatusItem.setNote(attrB_note.getText());
                            } else {
                                userStatusItem.setNote("");
                            }
                            Attribute attrB_selected = elementB.attribute("selected");
                            if (attrB_selected != null) {
                                userStatusItem.setSelected(Boolean.parseBoolean(attrB_selected.getText()));
                            } else {
                                userStatusItem.setSelected(false);
                            }
                            /* 是否可见 */
                            userStatusItem.setView(true);

                            userStatusItemMap.put(attrB_code.getText(), userStatusItem);
                        }
                    }// userStatusItem end
                }// userStatusItems end
                else if ("uploadCodeItems".equals(elementA.getName())) {
                    // 获取节点 uploadCodeItem
                    for (Iterator iterB = elementA.elementIterator(); iterB
                            .hasNext(); ) {
                        Element elementB = (Element) iterB.next();
                        if (elementB != null && "uploadCodeItem".equals(elementB.getName())) {
                            Attribute attrB_code = elementB.attribute("code");
                            Attribute attrB_text = elementB.attribute("text");

                            // create uploadCodeItem
                            UploadCodeItem uploadCodeItem = new UploadCodeItem(
                                    attrB_code.getText(), attrB_text.getText());

                            Attribute attrB_defText = elementB.attribute("defText");
                            if (attrB_defText != null) {
                                uploadCodeItem.setDefText(attrB_defText.getText());
                            } else {
                                uploadCodeItem.setDefText("");
                            }
                            Attribute attrB_note = elementB.attribute("note");
                            if (attrB_note != null) {
                                uploadCodeItem.setNote(attrB_note.getText());
                            } else {
                                uploadCodeItem.setNote("");
                            }

                            uploadCodeItemMap.put(attrB_code.getText(), uploadCodeItem);
                        }
                    }// uploadCodeItem end
                }


            }
        }//alice-coder end
    }

}
