package net.mayee.alice.service.admin.account;

import net.mayee.alice.dao.admin.account.RoleDao;
import net.mayee.alice.entity.admin.account.Role;
import net.mayee.alice.entity.admin.account.UserRole;
import net.mayee.common.utils.Encodes;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Service("RoleService")
public class RoleService {

    @Autowired
    private RoleDao roleDao;

    public Role getRole(String uuid) {
        return roleDao.get(uuid);
    }

    public void insertRole(Role role) {
        roleDao.insert(role);
    }

    public void updateRole(Role role) {
        roleDao.update(role);
    }

    public void removeRole(String uuid) {
        roleDao.remove(uuid);
    }

    public int count(Map<String, Object> parameters) {
        return roleDao.count(parameters);
    }

    public List<Role> searchByLimit(Map<String, Object> parameters) {
        return roleDao.searchByLimit(parameters);
    }

    public Role getByRoleKey(String roleKey) {
        return roleDao.getByRoleKey(roleKey);
    }

    public List<Role> getAll() {
        return roleDao.getAll();
    }

    public List<Role> getRolesByUser(String uid) {
        return roleDao.getRolesByUser(uid);
    }


    public String saveUserRole(Role role, String j) {
        List<String> errorList = new ArrayList<String>();
        String uid = role.getUuid();
        roleDao.removeUserRole(uid);

        com.alibaba.fastjson.JSONArray josnArray =
                com.alibaba.fastjson.JSON.parseArray(Encodes.unescapeHtml(j));
        for (Object obj : josnArray) {
            if (obj instanceof com.alibaba.fastjson.JSONObject) {
                com.alibaba.fastjson.JSONObject JSONObject = (com.alibaba.fastjson.JSONObject) obj;
                String val = JSONObject.getString("id");
                String user_uid = val.split("_")[0];
                String user_name = val.split("_")[1];

                    /* 角色数量检查 */
                boolean isContainThisRole = false;
                List<Role> roleChooseList = getRolesByUser(user_uid);
                if (roleChooseList.size() == 3) {
                    for (Role tempRole : roleChooseList) {
                        if (tempRole.getUuid().equals(uid)) {
                            isContainThisRole = true;
                            break;
                        }
                    }
                    if (!isContainThisRole) {
                        errorList.add(user_name);
                        continue;
                    }
                }

                UserRole ur = new UserRole();
                ur.setUserUuid(user_uid);
                ur.setRoleUuid(uid);
                roleDao.insertUserRole(ur);
            }
        }
        if (errorList.size() < 1) {
            return null;
        } else {
            return StringUtils.join(errorList, ",");
        }

    }

    public HashMap<String, String> getPermissionsByRole(String uid) {
        HashMap<String, String> map = new HashMap<String, String>();
        Role role = roleDao.get(uid);
        if (role != null && role.getPermissions() != null) {
            String[] perStr = role.getPermissions().split(",");
            for (String per : perStr) {
                String[] kv = per.split(":");
                if (StringUtils.isNotBlank(kv[0]) && StringUtils.isNotBlank(kv[1])) {
                    map.put(kv[0], kv[1].toUpperCase());
                }
            }
        }
        return map;
    }


}
