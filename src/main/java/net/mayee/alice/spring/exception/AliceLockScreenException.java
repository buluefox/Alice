package net.mayee.alice.spring.exception;

/**
 * Created by mayee on 16/1/29.
 */
public class AliceLockScreenException extends AliceException {

    public AliceLockScreenException() {
        super();
    }

    public AliceLockScreenException(String message) {
        super(message);
    }

    public AliceLockScreenException(Throwable cause) {
        super(cause);
    }

    public AliceLockScreenException(String message, Throwable cause) {
        super(message, cause);
    }

}
