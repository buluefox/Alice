package net.mayee.alice.common;

import java.io.File;

/**
 * 常量类
 *
 * 说明：
 * 以'DEF_'开头的常量：可在application.properties文件中覆盖其默认值；
 *
 */
public final class Definiens {

    private Definiens() {}

    /**
     * status values
     */
    public static final int UNAVA_VAL = 0;
    public static final int AVA_VAL = 1;
    
    /**
     * charset
     */
    public static final String CHARSET_UTF8 = "UTF-8";
    public static final String CHARSET_GBK = "GBK";

    public static final String MSG_ID = "msgid";
    public static final String MSG_DESC = "msgdesc";
    public static final String MSG_ERRORS = "msgerrors";

    /**
     * browser
     */
    public static final String USER_AGENT = "USER_AGENT";
    public static final String MOBILE = "MOBILE";
    public static final String PC = "PC";

    /**
     * err code
     */
    public static final String SUCCESS_DEFAULT = "SSSS";
    public static final String ERR_SYS_01 = "ERR_SYS_01";
    public static final String ERR_SYS_10 = "ERR_SYS_10";
    public static final String ERR_SYS_11 = "ERR_SYS_11";
    public static final String ERR_SYS_12 = "ERR_SYS_12";
    public static final String ERR_SYS_13 = "ERR_SYS_13";
    public static final String ERR_VALIDATEIMG_REFRESH_TOO_MUCH = "ERR_VALIDATEIMG_REFRESH_TOO_MUCH";
    public static final String ERR_VALIDATEIMG_CHECK_FAIL = "ERR_VALIDATEIMG_CHECK_FAIL";
    public static final String ERR_APPLICATION_CONFIG = "ERR_APPLICATION_CONFIG";
    public static final String ERR_MOBILE = "ERR_MOBILE";
    public static final String ERR_BROWSER = "ERR_BROWSER";

    /**
     * upload code
     */
    public static final String UC_SUCCESS = "UC_SUCCESS";
    public static final String UC_NOFILE = "UC_NOFILE";
    public static final String UC_TYPE = "UC_TYPE";
    public static final String UC_SIZE = "UC_SIZE";
    public static final String UC_ENTYPE = "UC_ENTYPE";
    public static final String UC_REQUEST = "UC_REQUEST";
    public static final String UC_IO = "UC_IO";
    public static final String UC_DIR = "UC_DIR";
    public static final String UC_UNKNOWN = "UC_UNKNOWN";

    /**
     * 接口提交方式
     */
    public static final String CONN_POST = "POST";
    public static final String CONN_GET = "GET";

    /**
     * 屏幕锁定判断标记
     */
    public static final int STATUS_CODE_LOCKSCREEN = 1;
    public static final int STATUS_CODE_UNLOCKSCREEN = 0;
    
    
    /**
     * user状态码
     */
    public static final int STATUS_CODE_USER_LOCKED = 0;//锁定
    public static final int STATUS_CODE_USER_SUPER_ADMIN = 1;//超级管理员
    public static final int STATUS_CODE_USER_ADMIN = 2;//管理员
    public static final int STATUS_CODE_USER_NORMAL = 3;//正常

    /**
     * 登陆时间
     */
    public static final String SESSIONKEY_LOGIN_LAST_TIME = "LAST_LOGIN_TIME";
    /* 默认最小登录间隔时间[单位：毫秒] */
    public static final long DEF_LOGIN_ACTION_MIN_TIME = 5000;

    /**
     * 验证码
     */
    public static final String SESSIONKEY_LOGIN_VALIDATEIMG = "LOGIN_VALIDATEIMG";
    public static final String SESSIONKEY_LOGIN_VALIDATEIMG_PATH = "LOGIN_VALIDATEIMG_PATH";
    public static final String SESSIONKEY_REFRESH_NUM = "REFRESH_NUM";
    public static final String SESSIONKEY_VALIDATEIMG_LAST_TIME = "VALIDATEIMG_LAST_TIME";
    /* 默认图片验证码刷新次数最大 */
    public static final int DEF_VALIDATEIMG_MAX_REFRESH_NUM = 10;
    /* 默认图片验证码重置时间[单位：毫秒] */
    public static final long DEF_VALIDATEIMG_ACTION_TIME = 30000;
    /* 默认图片验证码干扰线数量 */
    public static final int DEF_VALIDATEIMG_LINE_COUNT = 50;
    
    public static final String DEF_VALIDATEIMG_SAVEPATH_LOGIN = "validate" + File.separator + "login" + File.separator;
    

    
}
