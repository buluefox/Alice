package net.mayee.alice.entity.permission;

/**
 * Created by mayee on 15/10/18.
 */
public class Action {

    private String key;
    private String text;
    private String note = "";
    private String menuId;
    private String menuName;

    public Action(String key) {
        this.key = key;
    }

    public Action(String key, String menuId) {
        this.key = key;
        this.menuId = menuId;
    }

    public Action(String key, String text, String note, String menuId, String menuName) {
        this.key = key;
        this.text = text;
        this.note = note;
        this.menuId = menuId;
        this.menuName = menuName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getKey() {
        return key;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Action action = (Action) o;

        if (key != null ? !key.equals(action.key) : action.key != null) return false;
        return !(menuId != null ? !menuId.equals(action.menuId) : action.menuId != null);

    }

    @Override
    public int hashCode() {
        int result = key != null ? key.hashCode() : 0;
        result = 31 * result + (menuId != null ? menuId.hashCode() : 0);
        return result;
    }
}
