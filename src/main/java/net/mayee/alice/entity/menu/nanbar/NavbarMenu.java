package net.mayee.alice.entity.menu.nanbar;

import java.io.Serializable;

/**
 * Created by mayee on 15/9/16.
 */
public class NavbarMenu implements Serializable {

    private String id;
    private String name;
    private String messageKey;
    private String href;
    private String icon;
    private Boolean hadBottomLine = false;

    public NavbarMenu(String id, String name,String messageKey, String href, String icon) {
        this.id = id;
        this.name = name;
        this.messageKey = messageKey;
        this.href = href;
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public String getMessageKey() {
        return messageKey;
    }

    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Boolean getHadBottomLine() {
        return hadBottomLine;
    }

    public void setHadBottomLine(Boolean hadBottomLine) {
        this.hadBottomLine = hadBottomLine;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NavbarMenu that = (NavbarMenu) o;

        return id.equals(that.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
