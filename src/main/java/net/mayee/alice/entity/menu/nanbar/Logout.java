package net.mayee.alice.entity.menu.nanbar;

import java.io.Serializable;

/**
 * Created by mayee on 15/9/20.
 */
public class Logout implements Serializable {

    private String href;
    private String icon;

    public Logout(String href, String icon) {
        this.href = href;
        this.icon = icon;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Logout logo = (Logout) o;

        return href.equals(logo.href);

    }

    @Override
    public int hashCode() {
        return href.hashCode();
    }
}
