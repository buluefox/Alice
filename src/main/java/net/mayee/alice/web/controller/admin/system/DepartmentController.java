//package net.mayee.alice.web.controller.admin.system;
//
//import net.mayee.alice.entity.admin.account.Department;
//import net.mayee.alice.web.controller.base.BaseController;
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestMapping;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.util.*;
//
//@Controller
//public class DepartmentController extends BaseController {
//
//    @RequestMapping(value = "/sdm/acc/dep/L")
//    public String departmentList(Model model) {
//        return "account/department/departmentList";
//    }
//
//    @RequestMapping(value = "/acc/dep/ajax/L")
//    public void departmentListAjax(HttpServletResponse response, HttpServletRequest request) throws IOException {
//        Map<String, ArrayList<Department>> parent_depHM = new HashMap<String, ArrayList<Department>>();
//        JSONArray tree = new JSONArray();
//
//        List<Department> depList = this.getDepartmentService().getAll();
//        for (Department dep : depList) {
//            String parentDepUid = dep.getParentUid();
//            ArrayList<Department> subDepArray = new ArrayList<Department>();
//            if (parent_depHM.containsKey(parentDepUid)) {
//                subDepArray = parent_depHM.get(parentDepUid);
//            }
//            subDepArray.add(dep);
//            parent_depHM.put(parentDepUid, subDepArray);
//        }
//
//        Set<String> depSet = parent_depHM.keySet();
//        for (String parentDepUid : depSet) {
//            ArrayList<Department> array = (ArrayList<Department>) parent_depHM.get(parentDepUid);
//            for(Department dep : array){
//                JSONObject node = new JSONObject();
//                try {
//                    node.put("id", dep.getUuid());
//                    node.put("parent", dep.getParentUid());
//                    node.put("text", dep.getName());
//
//                    /* root node can not remove and update */
//                    if("#".equals(dep.getParentUid())){
//                        JSONObject state = new JSONObject();
//                        state.put("opened", true);
//                        state.put("selected", true);
//                        state.put("disabled", true);
//
//                        node.put("state", state);
//                    }
//
//                    tree.put(node);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//
//        System.out.print("Tree:"+tree.toString());
//
//        response.setContentType("application/json");
//        response.setHeader("Cache-Control", "no-store");
//        PrintWriter out = response.getWriter();
//        out.write(tree.toString());
//    }
//
//}
