package net.mayee.alice.web.controller.admin.system;

import net.mayee.alice.web.controller.base.BaseController;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController extends BaseController {

    @RequiresUser
	@RequestMapping(value = "/sdm/main")
	public String mainPage(Model model) {
		return "main";
	}
	
	

    

}
