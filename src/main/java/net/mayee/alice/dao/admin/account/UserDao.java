package net.mayee.alice.dao.admin.account;

import net.mayee.alice.entity.admin.account.User;
import net.mayee.alice.entity.admin.account.UserRole;
import net.mayee.alice.spring.annotation.MayeeBatisRepository;

import java.util.List;
import java.util.Map;


@MayeeBatisRepository
public interface UserDao {

	User get(String uuid);

	/**
     * 根据条件返回列表
     * @param parameters
     * @return
     */
    List<User> searchByLimit(Map<String, Object> parameters);
    
    int count(Map<String, Object> parameters);

	void insert(User user);

	void insertUserRole(UserRole userRole);

	void update(User user);

	void updatePassword(User user);

	void remove(String uid);

    void removeRolesByUser(String uid);
	
	User getByLoginName(String loginName);

	List<User> getAllNoAdmin();



    List<User> getUsersByRole(String uuid);
}
