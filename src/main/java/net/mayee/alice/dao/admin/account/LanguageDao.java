package net.mayee.alice.dao.admin.account;

import net.mayee.alice.entity.admin.account.Language;
import net.mayee.alice.spring.annotation.MayeeBatisRepository;

import java.util.List;
import java.util.Map;


@MayeeBatisRepository
public interface LanguageDao {

	Language get(int id);

	/**
     * 根据条件返回列表
     * @param parameters
     * @return
     */
    List<Language> searchByLimit(Map<String, Object> parameters);

	List<Language> getAll();
    
    int count(Map<String, Object> parameters);

	void insert(Language language);
	
	void update(Language language);

	void remove(String id);
	
}
