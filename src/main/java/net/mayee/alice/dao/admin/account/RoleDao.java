package net.mayee.alice.dao.admin.account;

import net.mayee.alice.entity.admin.account.Role;
import net.mayee.alice.entity.admin.account.UserRole;
import net.mayee.alice.spring.annotation.MayeeBatisRepository;

import java.util.List;
import java.util.Map;


@MayeeBatisRepository
public interface RoleDao {

    Role get(String uuid);

    /**
     * 根据条件返回列表
     *
     * @param parameters
     * @return
     */
    List<Role> searchByLimit(Map<String, Object> parameters);

    List<Role> getAll();

    int count(Map<String, Object> parameters);

    void insert(Role role);

    void update(Role role);

    void remove(String uuid);

    Role getByRoleKey(String roleKey);

    void removeUserRole(String uuid);

    void insertUserRole(UserRole userRole);

    List<Role> getRolesByUser(String uid);


}
